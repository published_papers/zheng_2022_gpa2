################################################################################
### Have a look at hapla data
################################################################################

###Set your work directory
setwd("E:/Nemawork/Projects/Qi_RNAseq/R/") 
workwd <- getwd()


###Load pre-made functions
    #uses eQTL pipeline functions https://git.wur.nl/mark_sterken/eQTL_pipeline
    #     transcriptomics functions https://git.wur.nl/mark_sterken/Transcriptomics.workbench
    #     genetic map functions https://git.wur.nl/mark_sterken/genetic.map
git_dir <- "E:/Nemawork/Projects_R_zone/Git/"
source(paste(git_dir,"/NEMA_functions/Loader.R",sep=""))

###Set data locations
#support_git_dir <- paste(git_dir,"/Kammenga_lab_experiments/eQTL_studies/let60_eQTL_files/",sep="")





################################################################################
###Dependencies
################################################################################

install <- FALSE
#.libPaths(.libPaths("C:/Program Files/R/R-3.5.3/library"))
if(install){
    install.packages("tidyverse")
    install.packages("colorspace")
    install.packages("RColorBrewer")
    install.packages("BiocManager")
    BiocManager::install(version = "3.12")
    BiocManager::install("limma")
    BiocManager::install("statmod")
    install.packages("gridExtra")
    install.packages("VennDiagram")
    install.packages("openxlsx")
    install.packages("rmarkdown")
    install.packages("ggpubr")
    install.packages("VariantAnnotation")
    install.packages("caret")
    install.packages("factoextra")
    install.packages("plotly")
    install.packages("rrBLUP")

}

###load
    library("colorspace")
    library("RColorBrewer")
    library(limma)
    library(gridExtra)
    library("VennDiagram")
    library(openxlsx)
    library("rmarkdown")
    library(tidyverse)
    library(ggpubr)
    library("VariantAnnotation")
    library("RcolorBrewer")
    library("caret")
    library("factoextra")  
    library(plotly)
    library(rrBLUP)

################################################################################
###Plotting theme, colours
################################################################################


###Set plotting theme
    presentation <- theme(axis.text.x = element_text(size=12, face="bold", color="black"),
                          axis.text.y = element_text(size=12, face="bold", color="black"),
                          axis.title.x = element_text(size=14, face="bold", color="black"),
                          axis.title.y = element_text(size=14, face="bold", color="black"),
                          strip.text.x = element_text(size=12, face="bold", color="black"),
                          strip.text.y = element_text(size=12, face="bold", color="black"),
                          plot.title = element_text(size=16, face="bold"),
                          panel.grid.minor = element_blank(),
                          panel.grid.major = element_line(colour="lightgrey"),
                          legend.position = "none",
                          panel.background = element_rect(fill = "white"),
                          panel.border = element_rect(colour="lightgrey",fill = NA))

    blank_theme <- theme(plot.background = element_blank(),
                         panel.grid.major = element_blank(),
                         panel.grid.minor = element_blank(),
                         panel.border = element_blank(),
                         panel.background = element_blank(),
                         axis.title.x = element_blank(),
                         axis.title.y = element_blank(),
                         axis.text.x = element_blank(),
                         axis.text.y = element_blank(),
                         axis.ticks = element_blank())


###Here you can set colours for plotting in theme using ggplot2
    #display.brewer.all()
    myColors <- c(brewer.pal(9,"Set1")[c(1,5,2,1)],"black")
    
    
    names(myColors) <- c("Des","Ser","Fes","-1","1")
    
    colScale <- scale_colour_manual(name = "Condition",values = myColors)
    fillScale <- scale_fill_manual(name = "Condition",values = myColors)



################################################################################
### vcf data
################################################################################
    
    data_location <- "W:/PROJECTS/NEMmasstorage/Datasets/RNA_seq/20201117_Potato_Qi/processed_data/G.pallida_variants"
    
    
    
    ## Load data ###
    vcf <- readVcf(
      file = paste(data_location,"/all.variants.filtered.vcf.gz",sep=""),
      param =  ScanVcfParam(
        fixed = "ALT",
        info = c("DP", "DP4"),
        geno = "GT"
      )
    )
    
    # Unpack
    calls <- geno(vcf)$GT
    # Parse columnames to contain only the sample names 
    colnames(calls) <- gsub(".bam","",gsub("results/sorted_reads/","",colnames(calls)))
    readDepths <- info(vcf)$DP
    
    
    ### Filter data ###
    # Set lower and upper cutoff on read depth
    depthCutoff <- c(50, 35000)
    
    # Select SNPs
    selected <- readDepths > depthCutoff[1]  & readDepths < depthCutoff[2]
    filteredCalls <- calls[selected,!grepl("M_",colnames(calls))]

    # Visualize read distribution with cut-offs
    plot.nu <- tibble(
                      depth = readDepths, 
                      label = ifelse(selected, "included", "excluded")
                    ) %>% 
                      plot_ly(
                        x = ~depth, 
                        color = ~label,
                        colors = RColorBrewer::brewer.pal(3, "Dark2")[c(2,1)],
                        type = "histogram",
                        xbins = list(size = 10)
                      ) %>% 
                      layout(
                        title = sprintf("SNP Read Depth (Frac Passed: %.3f)", sum(selected)/length(selected)),
                        xaxis = list(title = "Read Depth"),
                        margin = list(t = 60, r = 75, b = 70, l = 50, pad = 5)
                      )
    
    htmlwidgets::saveWidget(as_widget(plot.nu),"read_depth.html")
    #plotly_json(jsonedit = F, pretty = F) %>% capture.output(file = "figures/read_depth.json")


### Convert ###
# Summarize calls over all lines
myTable <- table(filteredCalls) %>% 
  as.data.frame() %>% 
  mutate(Freq = scales::label_number_si(accuracy = 0.1)(Freq)) %>% 
  pivot_wider(names_from = filteredCalls, values_from = Freq)
print(myTable)
write_tsv(myTable, file = "tables/genotype.tsv")

# Examine fraction of calls that are biallelic per line
myTable <- map_df(1:ncol(filteredCalls), function(i) {
  summary <- table(filteredCalls[,i])
  bind_cols(
    name = colnames(filteredCalls)[i], 
    frac = sum(summary[c("0/0", "1/1", "0/1")]) / sum(summary)
  )
}) %>% 
  arrange(-frac) %>% 
  transmute(
    Name = name,
    `SNPs Biallelic` = sprintf("%.2f%%", frac * 100)
  ) 
print(myTable, n = nrow(myTable))
write_tsv(myTable, "tables/frac_biallelic.tsv")

# Convert to a biallelic matrix
callMatrix <- filteredCalls %>% 
  as.data.frame() %>% 
  map_df(Vectorize(function(element) {
    if (element == "0/0") return(0)
    if (element == "1/1") return(1)
    if (element == "0/1") return(.5)
    else return(NA)
  }, USE.NAMES = F)) %>% 
  mutate(rowname = rownames(filteredCalls)) 


pdf("Rook_D383.pdf",width=6,height=4)
heatmap(t(apply(callMatrix[,-13],2,table)),scale = "none")
dev.off()

# Covert matrix to tidy format
tidyCallMatrix <- callMatrix %>% 
  na.omit() %>% 
  pivot_longer(cols = -rowname) %>% 
  pivot_wider(names_from = rowname, values_from = value) %>% 
  # Discard SNPs with zero variance as they contain no information
  dplyr::select(-caret::nearZeroVar(., freqCut = 100)) %>% 
  column_to_rownames("name")

### PCA ###
# Compute PCA
pca <- prcomp(tidyCallMatrix, scale = T, center = T)


            tmp <- prcomp(tidyCallMatrix,scale. = TRUE)
            summary(tmp)
            
            

            data.plot <- data.frame(cbind(name=rownames(tmp$x),tmp$x))
            
                for(i in 2:ncol(data.plot)){data.plot[,i] <- as.numeric(as.character(unlist(data.plot[,i])))}
            

            pdf("figure-Popstructure.pdf",width=6,height=6)
                ggplot(data.plot,aes(x=PC1,y=PC2,label=name)) + geom_point() +  
                ggrepel::geom_text_repel(max.overlaps = 100) + presentation +
                theme(legend.position = "none") + labs(x="PC1 (21.5%)",y="PC2 (10.9%)")
            dev.off()
            
            
            kinship.mat <- (t(tidyCallMatrix) - 0.5) *2
            kinship.mat <- A.mat(t(kinship.mat))
            
            tmp <- prcomp(kinship.mat,scale. = TRUE)
            summary(tmp)
            
            

            data.plot <- data.frame(cbind(name=rownames(tmp$rotation),tmp$rotation))
            
                for(i in 2:ncol(data.plot)){data.plot[,i] <- as.numeric(as.character(unlist(data.plot[,i])))}
            

            pdf("figure-Popstructure_kinship.pdf",width=6,height=6)
                ggplot(data.plot,aes(x=PC1,y=PC2,label=name)) + geom_point() +  
                ggrepel::geom_text_repel(max.overlaps = 100) + presentation +
                theme(legend.position = "none") + labs(x="PC1 (67.2%)",y="PC2 (11.8%)")
            dev.off()
                        
            
            
            
            # % of variance explained per component
explained <- pca$sdev^2 / sum(pca$sdev^2) * 100

# This can also be visualized
factoextra::fviz_screeplot(pca, addlabels = T, title = "PCA Scree Plot")
ggsave("figures/screeplot.jpg", height = 6, width = 8)

# Interactive PCA plot
plot.nu <- subplot(
                  # Create plots, one for PC1 & 2 and one for  PC3 & 4 using identical options
                  lapply(c(1,3), function(i) {
                    pca$x %>% 
                      as.data.frame() %>% 
                      rownames_to_column("line") %>% 
                      plot_ly(
                        x = as.formula(paste0("~PC", i)),
                        y = as.formula(paste0("~PC", i+1)),
                        text = ~line,
                        type = "scatter",
                        mode = "markers+text",
                        hoverinfo = "text",
                        textposition = "top center"
                      ) %>% 
                      layout(
                        xaxis = list(title = sprintf("PC%d (%.2f%%)", i, explained[i])),
                        yaxis = list(title = sprintf("PC%d (%.2f%%)", i+1, explained[i+1]))
                      )
                  }),
                  titleX = T, 
                  titleY = T, 
                  margin = 0.04
                ) %>%  
                  layout(
                    title = "<b>PCA:</b> genetic diveristy of the <i>Meloidogyne hapla</i> collection",
                    showlegend = F,
                    margin = list(l = 65, r = 15, t = 60, b = 65, pad = 5)
                  )
htmlwidgets::saveWidget(as_widget(plot.nu),"pca.html")


plotly_json(jsonedit = F, pretty = F) %>% capture.output(file = "figures/pca.json")

### Dendrogram ###
tidyCallMatrix %>% 
  # Add a space to the rownames
  rownames_to_column() %>% 
  mutate(rowname = paste0(" ", rowname)) %>% 
  column_to_rownames() %>% 
  # Calculate distances
  dist(method = "euclidean") %>% 
  hclust(method = "complete") %>% 
  as.dendrogram() %>% 
  # Visualize
  factoextra::fviz_dend(
    main = "M. hapla collection dendrogram (euclidean, complete)",
    horiz = T,
    labels_track_height = 50
  )
ggsave("figures/dendrogram.jpg", height = 6, width = 8)


